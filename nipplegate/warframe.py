import os.path
import re
from collections import namedtuple

import feedparser
from twisted.internet import task

from libs.util import CommandHandler


class WarframeHandler(CommandHandler):
    def __init__(self, bot):
        super(WarframeHandler, self).__init__()
        self.bot = bot
        self.commands.update({
            r"^alerts overview$": self.alerts_overview,
            r"^alerts filter .*$": self.update_filter
        })
        self.l = task.LoopingCall(self.update_alerts)

        self.bot.on('join', lambda _: self.l.start(60))

    def alerts_overview(self, arguments, user):
        alerts = self.load_stored_alerts()

        return ["%s: %s" % (alert.alert_type.capitalize(), alert.description) for alert in alerts.values()]

    def update_filter(self, arguments, user):
        _, filter_query = arguments.split(' ', 1)

        if not os.path.exists('listeners.txt'):
            with open('listeners.txt', 'a'):
                os.utime('listeners.txt', None)

        with open('listeners.txt', 'r') as f:
            listeners = f.readlines()

        with open('listeners.txt', 'w') as f:
            user_found = False
            user_listener_data = '%s*%s\n' % (user, filter_query)
            for listener in listeners:
                if listener.split('*')[0] == user:
                    user_found = True
                    if not filter_query == 'clear':
                        f.write(user_listener_data)
                else:
                    f.write(listener)
            if not user_found:
                f.write(user_listener_data)

        return "Filter updated."

    def load_filters(self):
        with open('listeners.txt', 'r') as f:
            return [Listener(*l.split('*')) for l in f.readlines()]

    def update_alerts(self):
        latest_alerts = self.fetch_alerts()
        previous_alerts = self.load_stored_alerts()
        self.store_alerts(latest_alerts)

        new_alerts = self.difference_alerts(latest_alerts, previous_alerts)

        self.broadcast_alerts(new_alerts)

    def fetch_alerts(self):
        feed = feedparser.parse("http://content.warframe.com/dynamic/rss.php")
        new_alerts = {}
        for entry in feed['entries']:
            alert = self.to_alert(entry)
            new_alerts[alert.key] = alert
        return new_alerts

    def store_alerts(self, latest_alerts):
        if os.path.exists('alerts.txt'):
            os.remove('alerts.txt')
        with open('alerts.txt', 'w') as f:
            for alert in latest_alerts.values():
                f.write('|'.join([alert.key, alert.alert_type, alert.description]) + '\n')

    def load_stored_alerts(self):
        if not os.path.exists('alerts.txt'):
            return {}

        stored_alerts = {}

        with open('alerts.txt', 'r') as f:
            for line in f.readlines():
                alert = Alert(*line.split('|'))
                stored_alerts[alert.key] = alert
        return stored_alerts

    def broadcast_alerts(self, new_alerts):
        listeners = self.load_filters()
        for listener in listeners:
            for alert in new_alerts.values():
                if self.is_alert_sendable(alert, listener.filter):
                    self.bot.send_message("WF: %s" % alert.description, listener.user)

    def is_alert_sendable(self, alert, filter_query):
        filters = filter_query.split(' ')
        for f in filters:
            parsed = re.split('([^a-zA-Z]+)', f, 1)
            alert_type, operator, queries = parsed[0], parsed[1], parsed[2].replace('\n','').split(',')

            print queries

            if not alert.alert_type == alert_type:
                continue

            if operator == '=':
                return any([q.lower() in alert.description.lower() for q in queries])
            elif operator == '!=':
                return not any([q.lower() in alert.description.lower() for q in queries])

        return False

    def difference_alerts(self, latest_alerts, previous_alerts):
        new_alerts = {}

        for key in latest_alerts.keys():
            if key not in previous_alerts:
                new_alerts[key] = latest_alerts[key]

        return new_alerts

    def to_alert(self, entry):
        key = entry['id'].rsplit('/', 1)[1]
        alert_type = entry['author']
        if alert_type == 'Outbreak':
            alert_type = 'invasion'
        description = entry['title']
        return Alert(key, alert_type.lower(), description)


Alert = namedtuple('Alert', ['key', 'alert_type', 'description'])
Listener = namedtuple('Listener', ['user', 'filter'])
