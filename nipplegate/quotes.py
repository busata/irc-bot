from collections import namedtuple

import psycopg2
import psycopg2.extras

from libs.util import CommandHandler

Quote = namedtuple('Quote', ['quote_id', 'message', 'added_by', 'added_on'])


class QuoteCommands(CommandHandler):
    def __init__(self, bot):
        super(QuoteCommands, self).__init__()
        self.quote_repository = QuoteRepository()
        self.commands.update({
            r"^$": self.get_random_quote,
            r"^get [0-9]+$": self.get_single_quote,
            r"^get [0-9]+-[0-9]+$": self.get_multiple_quotes,
            r"^get last$": self.get_last_quote,
            r"^get random$": self.get_random_quote,
            r"^add.*": self.add_single_quote,
            r"^remove [0-9]+$": self.remove_single_quote,
            r"^search [a-zA-Z0-9\s]+$": self.search_quotes,
        })

    def get_random_quote(self, arguments, user):
        quote = self.quote_repository.get_random_quote()

        if quote:
            return "%s : %s" % (quote.quote_id, quote.message)
        else:
            return "There are no quotes!"

    def get_single_quote(self, arguments, user):
        quote = self.quote_repository.get_single_quote(arguments)
        if quote:
            return "%s : %s" % (quote.quote_id, quote.message)
        else:
            return 'No quote with that id.'

    def get_multiple_quotes(self, arguments, user):
        start_quote_id, _, end_quote_id = arguments.partition("-")

        if int(start_quote_id) > int(end_quote_id):
            return "Invalid range, usage: a-b (a<b)!"

        if abs(int(start_quote_id) - int(end_quote_id)) > 5:
            return "Excess flood! (Too many quotes requested!)"

        quotes = self.quote_repository.get_multiple_quotes(start_quote_id, end_quote_id)

        if quotes:
            return ["%s : %s" % (quote.quote_id, quote.message) for quote in quotes]
        else:
            return "There seem to be no quotes in that range!"

    def get_last_quote(self, arguments, user):
        quote = self.quote_repository.get_last_quote()
        if quote:
            return "%s : %s" % (quote.quote_id, quote.message)
        else:
            return "No quote found!"

    def add_single_quote(self, arguments, user):
        quote_id = self.quote_repository.add_single_quote(arguments, user)

        return "Quote added with id: %s" % quote_id

    def remove_single_quote(self, arguments, user):
        deleted_quote = self.quote_repository.remove_single_quote(arguments)

        return ["The following quote was deleted:", deleted_quote.message]

    def search_quotes(self, arguments, user):
        results = self.quote_repository.search_quote(arguments)
        if results:
            quotes = ["%s : %s" % (quote.quote_id, quote.message) for quote in results]
            if len(results) > 4:
                quotes = quotes[:3]

                quote_ids = ""
                for quote in results[3:]:
                    quote_ids += "%s, " % (quote.quote_id,)
                quotes.append("-- More quotes: %s" % quote_ids[:-2])
                return quotes
            else:
                return quotes
        else:
            return ["No quotes found"]


class BaseRepository():
    connection = psycopg2.connect(user="nipplegate", password="2aWaPU9u")

    def database(commit=False):
        def decorator(func):
            def inner(*args, **kwargs):
                cursor = BaseRepository.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

                kwargs['cursor'] = cursor
                response = func(*args, **kwargs)

                if commit:
                    BaseRepository.connection.commit()

                cursor.close()

                return response

            return inner

        return decorator

    database = staticmethod(database)


class QuoteRepository(BaseRepository):
    def create_quote_from_result(self, result):
        quote = Quote(result[0], result[1], result[2], result[3])
        return quote

    @BaseRepository.database()
    def get_single_quote(self, quote_id, cursor):
        cursor.execute('SELECT quote_id,quote,added_by,added_on FROM "Quotes" WHERE quote_id = %s', (quote_id,))
        result = cursor.fetchone()

        if result:
            return self.create_quote_from_result(result)
        else:
            return None

    @BaseRepository.database()
    def add_single_quote(self, quote, nick, cursor):

        cursor.execute(
            'INSERT INTO "Quotes" (quote_id,quote,added_by,added_on) VALUES (DEFAULT, %s, %s, DEFAULT) RETURNING quote_id',
            (quote, nick))

        quote_id = cursor.fetchone()[0]

        return quote_id

    @BaseRepository.database()
    def get_random_quote(self, cursor):
        cursor.execute('SELECT quote_id,quote,added_by,added_on FROM "Quotes" ORDER BY RANDOM() LIMIT 1')

        result = cursor.fetchone()

        if result:
            quote = self.create_quote_from_result(result)
            return quote
        else:
            return None

    @BaseRepository.database()
    def get_last_quote(self, cursor):
        cursor.execute('SELECT * FROM "Quotes" WHERE quote_id=(SELECT MAX(quote_id) FROM "Quotes")')

        result = cursor.fetchone()

        if result:
            quote = self.create_quote_from_result(result)
            return quote
        else:
            return None

    @BaseRepository.database()
    def remove_single_quote(self, quote_id, cursor):
        cursor.execute('DELETE FROM "Quotes" WHERE quote_id=%s RETURNING *', (quote_id,))

        result = cursor.fetchone()

        if result:
            return self.create_quote_from_result(result)
        else:
            return None

    @BaseRepository.database()
    def search_quote(self, query, cursor):
        query = "%" + query + "%"
        cursor.execute('SELECT * FROM "Quotes" WHERE quote ILIKE %s', (query,))

        results = cursor.fetchall()

        if results:
            quotes_found = len(results)
            if quotes_found == 1:
                return [self.create_quote_from_result(results[0])]
            elif quotes_found:
                return [self.create_quote_from_result(result) for result in results]

    @BaseRepository.database()
    def get_multiple_quotes(self, start_quote_id, end_quote_id, cursor):
        cursor.execute('SELECT * FROM "Quotes" WHERE quote_id >= %s AND quote_id <= %s', (start_quote_id, end_quote_id))

        results = cursor.fetchall()

        if results:
            quotes = [self.create_quote_from_result(result) for result in results]
            return quotes
        else:
            return None
