import datetime as dt
from collections import defaultdict

from libs.util import CommandHandler, normalize_username


class NotesHandler(CommandHandler):
    def __init__(self, bot):
        super(NotesHandler, self).__init__()
        self.bot = bot
        self.bot.on('msg', self.check_notes)
        self.commands.update({
            r"^leave\s+.*$": self.store_note,
            r"^show$": self.show_notes
        })

        self.notes = defaultdict(list)
        self.reminders = {}

    def store_note(self, arguments, user):
        recipient, message = arguments.split(' ', 1)
        recipient = normalize_username(recipient)

        self.notes[recipient].append("%s (Left by: %s)" % (message, user))

        return "Note stored."

    def check_notes(self, _, user):
        normalized_user = normalize_username(user)

        if normalized_user in self.notes:
            messages = self.notes[normalized_user]
            if messages and self.can_remind(normalized_user):
                self.bot.send_message("You have notes, type .note show to display", user)
                self.set_reminder(normalized_user)

    def can_remind(self, user):
        if user not in self.reminders:
            return True

        now = dt.datetime.now()
        last_reminder = self.reminders[user]

        return (now - last_reminder).total_seconds() > 60 * 60

    def set_reminder(self, user):
        self.reminders[user] = dt.datetime.now()

    def show_notes(self, arguments, user):
        user = normalize_username(user)

        if user in self.notes:
            messages = self.notes[user]
            if messages and len(messages) > 0:
                self.notes[user] = []
                return messages

        return "Opening the mailbox reveals.... nothing, sorry!"
