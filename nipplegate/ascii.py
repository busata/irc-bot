from libs.util import CommandHandler


class AsciiHandler(CommandHandler):
    def __init__(self, bot):
        super(AsciiHandler, self).__init__()
        self.commands.update({
            r"^$": self.ascii,
        })

    def ascii(self, arguments, user):
        return ['            __',
                '           / _)',
                '    .-^^^-/ /',
                ' __/       /',
                '<__.|_|-|_|']
