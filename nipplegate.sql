--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Quotes; Type: TABLE; Schema: public; Owner: nipplegate; Tablespace: 
--

CREATE TABLE "Quotes" (
    quote_id integer NOT NULL,
    quote text NOT NULL,
    added_by text NOT NULL,
    added_on timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."Quotes" OWNER TO nipplegate;

--
-- Name: TABLE "Quotes"; Type: COMMENT; Schema: public; Owner: nipplegate
--

COMMENT ON TABLE "Quotes" IS 'This table contains all the Quotes!';


--
-- Name: Quotes_quote_id_seq; Type: SEQUENCE; Schema: public; Owner: nipplegate
--

CREATE SEQUENCE "Quotes_quote_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Quotes_quote_id_seq" OWNER TO nipplegate;

--
-- Name: Quotes_quote_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nipplegate
--

ALTER SEQUENCE "Quotes_quote_id_seq" OWNED BY "Quotes".quote_id;


--
-- Name: tsw_Characters; Type: TABLE; Schema: public; Owner: nipplegate; Tablespace: 
--

CREATE TABLE "tsw_Characters" (
    character_id integer NOT NULL,
    name text NOT NULL,
    user_name text NOT NULL
);


ALTER TABLE public."tsw_Characters" OWNER TO nipplegate;

--
-- Name: TABLE "tsw_Characters"; Type: COMMENT; Schema: public; Owner: nipplegate
--

COMMENT ON TABLE "tsw_Characters" IS 'Contains the characters for a user';


--
-- Name: tsw_Characters_character_id_seq; Type: SEQUENCE; Schema: public; Owner: nipplegate
--

CREATE SEQUENCE "tsw_Characters_character_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tsw_Characters_character_id_seq" OWNER TO nipplegate;

--
-- Name: tsw_Characters_character_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nipplegate
--

ALTER SEQUENCE "tsw_Characters_character_id_seq" OWNED BY "tsw_Characters".character_id;


--
-- Name: tsw_DungeonLockouts; Type: TABLE; Schema: public; Owner: nipplegate; Tablespace: 
--

CREATE TABLE "tsw_DungeonLockouts" (
    lockout_id integer NOT NULL,
    character_id integer,
    dungeon_id integer,
    locked_minutes integer DEFAULT 960,
    added_on timestamp without time zone DEFAULT now()
);


ALTER TABLE public."tsw_DungeonLockouts" OWNER TO nipplegate;

--
-- Name: tsw_DungeonLockouts_lockout_id_seq; Type: SEQUENCE; Schema: public; Owner: nipplegate
--

CREATE SEQUENCE "tsw_DungeonLockouts_lockout_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tsw_DungeonLockouts_lockout_id_seq" OWNER TO nipplegate;

--
-- Name: tsw_DungeonLockouts_lockout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nipplegate
--

ALTER SEQUENCE "tsw_DungeonLockouts_lockout_id_seq" OWNED BY "tsw_DungeonLockouts".lockout_id;


--
-- Name: tsw_Dungeons; Type: TABLE; Schema: public; Owner: nipplegate; Tablespace: 
--

CREATE TABLE "tsw_Dungeons" (
    dungeon_id integer NOT NULL,
    name text NOT NULL,
    abbreviation text NOT NULL,
    lockout_time integer NOT NULL
);


ALTER TABLE public."tsw_Dungeons" OWNER TO nipplegate;

--
-- Name: tsw_Dungeons_dungeon_id_seq; Type: SEQUENCE; Schema: public; Owner: nipplegate
--

CREATE SEQUENCE "tsw_Dungeons_dungeon_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tsw_Dungeons_dungeon_id_seq" OWNER TO nipplegate;

--
-- Name: tsw_Dungeons_dungeon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nipplegate
--

ALTER SEQUENCE "tsw_Dungeons_dungeon_id_seq" OWNED BY "tsw_Dungeons".dungeon_id;


--
-- Name: quote_id; Type: DEFAULT; Schema: public; Owner: nipplegate
--

ALTER TABLE ONLY "Quotes" ALTER COLUMN quote_id SET DEFAULT nextval('"Quotes_quote_id_seq"'::regclass);


--
-- Name: character_id; Type: DEFAULT; Schema: public; Owner: nipplegate
--

ALTER TABLE ONLY "tsw_Characters" ALTER COLUMN character_id SET DEFAULT nextval('"tsw_Characters_character_id_seq"'::regclass);


--
-- Name: lockout_id; Type: DEFAULT; Schema: public; Owner: nipplegate
--

ALTER TABLE ONLY "tsw_DungeonLockouts" ALTER COLUMN lockout_id SET DEFAULT nextval('"tsw_DungeonLockouts_lockout_id_seq"'::regclass);


--
-- Name: dungeon_id; Type: DEFAULT; Schema: public; Owner: nipplegate
--

ALTER TABLE ONLY "tsw_Dungeons" ALTER COLUMN dungeon_id SET DEFAULT nextval('"tsw_Dungeons_dungeon_id_seq"'::regclass);


--
-- Data for Name: Quotes; Type: TABLE DATA; Schema: public; Owner: nipplegate
--

COPY "Quotes" (quote_id, quote, added_by, added_on) FROM stdin;
1	Spectra: i googled manboobs for asp and there were some impressive ones lol	Stage	2013-02-07 22:57:15.770688+01
2	<@[KP]> haha, warning in the motherboard manual; "Thermal paste is inedible."	Stage	2013-02-07 22:57:15.770688+01
3	<eBusata> toss the turtle & me however!	Stage	2013-02-07 22:57:15.770688+01
4	<Fuzzbot> [TETN] Xzentoren: wanna pee on my towel too maj ?	Stage	2013-02-07 22:57:15.770688+01
5	<Fuzzbot> [DR] Battlespork: ahaha, bitbucket <Fuzzbot> [DR] Battlespork: problem is I can't be as naturally rude as stage	Stage	2013-02-07 22:57:15.770688+01
6	[Divine Retribution] Fuzzbot: [IRC] [KP]: what is it about computers that turns people who can use a microwave into people who can't use a kettle	Stage	2013-02-07 22:57:15.770688+01
7	Vinger: do you sit down in a corner leaning against the wall looking all sadfaced when you masturbate too ?	busata	2013-02-07 22:57:15.770688+01
8	<@[KP]> he will fly to germany and beat you with metal pants	Stage	2013-02-07 22:57:15.770688+01
9	Enaroy: ...the unclean toilets. Sethic: tho, the one guy who's cellphone fell in and is sitting outside crying kinda makes you go "haha"	Stage	2013-02-07 22:57:15.770688+01
10	Fiddles: Remind me why I moved to the city... Anelauri: airaid! Anelauri: shopchecking! Anelauri: tradeskills galore! Anelauri: socializing in the pool...? <@|KP|> you're a big dork, ferl :P Anelauri: enormous!	Stage	2013-02-07 22:57:15.770688+01
11	Fixrocker: solid is complaining about all the people trying to talk to him ;p Fixrocker: [Transcendence] Nerf: [Jjin]: Got time for some nooby tower timer questions? [Pernille]: want to host an ipande raid later? [Soyala]: Mmhmm Big box with ducks on it	Stage	2013-02-07 22:57:15.770688+01
12	Give this man your finest trouser arouser.	bocker	2013-02-07 22:57:15.770688+01
13	Given his patriotic interpretation of adultery, Gingrich would surely concede that Bill Clinton was a great president and apologize for his previous attacks.	bocker	2013-02-07 22:57:15.770688+01
14	That's not me, that's god.  The all-knowing, all-loving creator who made all things, and hates some of them.	bocker	2013-02-07 22:57:15.770688+01
15	Dippin' Dots has filed for bankruptcy. Dippin' Dots was the ice cream of the future. And if Dippin' Dots is now in the past, that means we're living in some bizarre post-future timescape. Nation this leaves a gap in the future desert continuum. What we we eat in the future now, hover pie?	bocker	2013-02-07 22:57:15.770688+01
16	Italian debt threatens to swamp the Eurozone. I knew they shouldn't have offered unlimited breadsticks	bocker	2013-02-07 22:57:15.770688+01
17	Her face looks like 5 miles of bad road	bocker	2013-02-07 22:57:15.770688+01
18	Today a young man on acid realized that all matter is merely energy condensed to a slow vibration. That we are all one consciousness experiencing itself subjectively. There is no such thing as death, life is only a dream, and we are the imagination of ourselves. Here's Tom with the weather.	bocker	2013-02-07 22:57:15.770688+01
19	By issue two [of People magazine], the format had shifted to pictures of largely unbearded movie and TV stars getting into and out of cars.	bocker	2013-02-07 22:57:15.770688+01
20	And let's be honest for a moment.  If you found a large lizard in your house, what would you do?  FIND SOMETHING TO RACE IT.  It's just human nature.	bocker	2013-02-07 22:57:15.770688+01
21	Hastily assembled in 1927 and still running today, the Cyclone remains a Coney Island favorite among wooden-coaster enthusiasts.  It is consistently ranked "most frighteningly decrepit" and "most bruisey" by Dangerous Attractions magazine.	bocker	2013-02-07 22:57:15.770688+01
22	Being a juror is perhaps the greatest privilege of citizenship.  It is better than voting.  Better than free speech.  And naturally, it is much, much better than habeas corpus.  For while those rights can be taken away, EVERYONE in the United States is entitled to have their fate decided by an untrained panel of resentful people chosen at random.	bocker	2013-02-07 22:57:15.770688+01
23	Twitter didn't save Iran, Iran saved Twitter.  If you had asked Twitter users a week ago they would have said iRan is the new treadmill from Apple.	bocker	2013-02-07 22:57:15.770688+01
24	Kids these days are second and third-generation lazy. Their parents tell them "When I was your age, we didn't sit around and watch football. We played football...on Nintendo."	bocker	2013-02-07 22:57:15.770688+01
25	Under the Patriot Act George Bush has reclassified all Vietnam veterans as "show offs".	bocker	2013-02-07 22:57:15.770688+01
26	Only humans could visit an infinite void and leave it cluttered.	bocker	2013-02-07 22:57:15.770688+01
27	By the way, you know what makes a really great business man?  When your father has $400 million and dies.  Or, as Trump calls it, "The Art of the Deal."	bocker	2013-02-07 22:57:15.770688+01
28	She was the type who would cockblock endangered pandas at the zoo.	bocker	2013-02-07 22:57:15.770688+01
29	The men the American people admire most extravagantly are the most daring liars; the men they detest most violently are those who try to tell them the truth. A Galileo could no more be elected President of the United States than he could be elected Pope of Rome.	bocker	2013-02-07 22:57:15.770688+01
30	A recent study shows that women care more about how a penis looks than how big it is.  Unfortunately, they like it to look big.	bocker	2013-02-07 22:57:15.770688+01
31	I actually shoot my porn in sepiatone and make it look old-timey so if people see it they'll think it's my grandfather and not me.	bocker	2013-02-07 22:57:15.770688+01
32	<@|KP|> how did you manage to lose a keyboard? <minizav> found it <minizav> keyboard was in the kitchen, mouse i dont want to say where i found :P <@|KP|> handbag! <@|KP|> fridge! <@|KP|> dirty washing bin! <minizav> you're close :P <@|KP|> oven? <@|KP|> dishwasher <minizav> sock drawer..	Stage	2013-02-07 22:57:15.770688+01
33	Remember what France did for America.  If it wasn't for France, you'd all still be speaking English...properly!	bocker	2013-02-07 22:57:15.770688+01
34	I work harder than a cat trying to bury a turd on a marble floor.	bocker	2013-02-07 22:57:15.770688+01
35	Sushilover: i only like women Hypnotik: but what about your gay lover? Genesee: You hit on more ment han you do women Asp, at least in here Sushilover: zero is just a friend that i share bodily fluids with	Stage	2013-02-07 22:57:15.770688+01
36	Glenn Beck has a plan for these 'Scraping-by loaders'- "If you pay nothing, you're the one who has to serve in the military'.  From now on, wars will no longer be fought disproportionately by the very rich.	bocker	2013-02-07 22:57:15.770688+01
37	The special olympics is to winners as Fox News is to experts - if you show up, you are one.	bocker	2013-02-07 22:57:15.770688+01
38	The president is just back from Mexico, which, as you know, is America's leading supplier of Mexicans.  Although the Japanese are catching up.	bocker	2013-02-07 22:57:15.770688+01
39	Within 3 days of [Bernie Madoff] arriving in jail he'd launched a prison fund that was yielding 2 handjobs per every pack of cigarettes invested, obviously a ridiculous rate of return that was soon exposed as a massive fraud. Meaning that sadly now there are scores of hard working inmates who will never see the hundreds, and in some cases, thousands of handjobs they were counting on.	bocker	2013-02-07 22:57:15.770688+01
40	[Quilluck]: I eat babies and shit them out in their grown adult for, thats why people talk shit	bocker	2013-02-07 22:57:15.770688+01
41	[Team] Quilluck: must..resist..ur..mom..comments..	bocker	2013-02-07 22:57:15.770688+01
42	Gilean: You will kill big things, take their weapons and armor, and use them to kill bigger big things. If you're male, and you probably are, your sperm count will rise.	bocker	2013-02-07 22:57:15.770688+01
43	Steeltown: um dust you realize im omni right?  Dusteen: i forgot your factioned :(  Steeltown: shall we fake LD? :p lol  [Team] Steeltown: um...explosive diahrea i gotta run  You are no longer in a team.  [Wolfnet] Steeltown: id say i did that with tact and grace	bocker	2013-02-07 22:57:15.770688+01
44	[TETN] Tetnbot: [IRC] ^KP^: wonder if a team of 6 crats could do a 220 hard [TETN] Zavy: how would they kill the last charm? :P [TETN] Sethic: sofc crat just alpha's last one	bocker	2013-02-07 22:57:15.770688+01
45	[Team] Steeltown: clionius? [Team] Steeltown: i think he pulled a helen keller on us. blind and deaf [Team] Whataforce: ./hunt afk? [Team] Clionius: brb [Team] Steeltown: ... [Team] Whataforce: lol	bocker	2013-02-07 22:57:15.770688+01
46	The difference between playing the stock market and the horses is that one of the horses must win.	bocker	2013-02-07 22:57:15.770688+01
47	Most people work just hard enough not to get fired and get paid just enough money not to quit.	bocker	2013-02-07 22:57:15.770688+01
48	I found there was only one way to look thin, hang out with fat people.	bocker	2013-02-07 22:57:15.770688+01
49	Doctor's Dictionary - add: This is short for 'additional monsters needed' and is used when the monster you are fighting is an easy kill and the doc feels the team can handle more. When you hear this, run into ajoining rooms and pull more monsters into the fight.	bocker	2013-02-07 22:57:15.770688+01
50	Doctor's Dictionary - afk: This common term stands for "Attack, Fight, Kill" and is the doc's way of telling you to go pull mobs.	bocker	2013-02-07 22:57:15.770688+01
51	Doctor's Dictionary - brb: An acronym, this is short for "Boss Room Battle," and is a doc's way of telling the team it's time to fight the dungeon or mission boss. Note that you will also sometimes see "afk brb," meaning the same thing.	bocker	2013-02-07 22:57:15.770688+01
52	Doctor's Dictionary - out of nano: If your doc says this after a fight, make sure you hit her with your QL 20 nano recharger berfore you run three rooms down and start another major fight.	bocker	2013-02-07 22:57:15.770688+01
53	Doctor's Dictionary - WTF!?!: This means "Way To Fight" and is used to complement you on your brilliant tactical choices.	bocker	2013-02-07 22:57:15.770688+01
54	Doctor's Dictionary - puller: The team's puller has the important job of "pulling" the team into fights. Usually this is accomplished by having an Agent with no evades Aimed Shot a social mob for 1/2 it's life.	bocker	2013-02-07 22:57:15.770688+01
55	[Hellcom] Brock93: all who think are enuf strong can check your pwer to get on best platform and who gona be last thet  gona won (some price) :)	bocker	2013-02-07 22:57:15.770688+01
56	[Team] Ceirwyn: lol, did we all alpha that zix? [Team] Ierwin: i did [Team] Sethic: I know I did	bocker	2013-02-07 22:57:15.770688+01
57	[Team] Ontherag: yes yes dont go all avon emo on me here goth :p [Team] Tehvamp: TOO LATE [Team] Tehvamp: CRAWLLLLING INNNNNN MY SKINNNNNNNN [Team] Tehvamp: theeeeseeeee wounnnnnds they willllll not heallllll [Team] Tehvamp: brb need more black eyeliner	bocker	2013-02-07 22:57:15.770688+01
58	[Team] Quilluck: also.. get me a sammich and a beer [Team] Tigist: want me to suck your cocke and rub your feet too? [Team] Ontherag: wow, full service! [Team] Sidira: no I think he wants anal penentration with the mop [Team] Tigist: get the handle and the ky [Team] Quilluck: sounds good, Tig	bocker	2013-02-07 22:57:15.770688+01
59	[Team] Theppy: ya know, Croaker of Solitude and Croaker of Desolation are hangin in the same room... ya think they could just get together	bocker	2013-02-07 22:57:15.770688+01
60	[Clan OOC] Drstabngrab: god i love my pet [Clan OOC] Lordstage: You can go to prison for that in some countries	bocker	2013-02-07 22:57:15.770688+01
61	[TETN] Sethic: have some flair for the fine arts Stage [TETN] Sethic: Now is the winter of our discountsupplebots...	bocker	2013-02-07 22:57:15.770688+01
62	[TETN] Sethic: fuck you cantanky [TETN] Sethic: stupid ugly hat [Fuzzbot] Steeltown: now WEAR it! [TETN] Sethic: I delete those on the spot just to avoid running into people wearing those after buying em from me	bocker	2013-02-07 22:57:15.770688+01
63	[Hellcom] Scoope: krause touched my wiener! [Hellcom] Krause: did not >:( [Hellcom] Scoope: Im not complaining!	bocker	2013-02-07 22:57:15.770688+01
64	[TETN] Lolith: england had a huuuuge empire once, we stole everyone's food & ruined it	bocker	2013-02-07 22:57:15.770688+01
65	To [alcoydel]: lol i kept my pets when i zoned up To [Alcoydel]: still buffed and everything! [Alcoydel]: never happend to you before? To [Alcoydel]: not to my recollection [Alcoydel]: i spend hours trying to reproduce that To [Alcoydel]: well i was spinning in circles with one leg in the air with a spoon of mashd potatos balanced on my nose chanting zimbabwean rain dance, worth a try next time your at it	bocker	2013-02-07 22:57:15.770688+01
66	Even if you ignore the illustration, there are only eight possible ways to run a rectangular card through a narrow slot.  But some customers seem to be able to beat all statistical probability by randomly swiping their card the same seven wrong ways over and over again.	bocker	2013-02-07 22:57:15.770688+01
67	It's very safe to be an inanimate object, but the carbon molecules who were our ancestors chose otherwise, and having once set upon a course of devouring things, we must submit to having other things occasionally attempt to devour us.	bocker	2013-02-07 22:57:15.770688+01
68	Paraguay's climate is almost identical to Florida's but more comfortable since nobody has to wear a giant mouse costume to make a living.	bocker	2013-02-07 22:57:15.770688+01
152	To [Biancha]: your like the team mascot tho, we need you here for morale reasons [Biancha]: gooooooooooo team!!! rah rah pom poms etc.	bocker	2013-02-07 22:57:15.770688+01
69	Whataforce: well south park said it best in the end our choice come down to a giant deuche or a terd sandwich so choose wisely this year folks Ontherag: you spelled douche wrong. also, turd. no child left behind obviously didnt work for you Whataforce: i went through public school what do ya wnat from me lol	bocker	2013-02-07 22:57:15.770688+01
70	[Transcendence] Snippetty: lets go have sex five times gayer than elton john danda [Transcendence] Transchat: [Guest] Ophiuchus: Five times gayer? How is that even possible? o.O	bocker	2013-02-07 22:57:15.770688+01
71	[On Iraq] When you want to make it clear to the rest of the world that you are not an imperialist, the best countries to have with you are Britain and Spain.	bocker	2013-02-07 22:57:15.770688+01
72	I love how we morphed Bin Laden into Hussein.  I mean, I'm glad he's gone, but the Joker is not the Riddler.	bocker	2013-02-07 22:57:15.770688+01
73	[Team] Sethic: but nodrop stuff is not non no-droppable	bocker	2013-02-07 22:57:15.770688+01
74	<@SethOn> evening all Sunkist: hi Seth! Spectra: sup seth! <@SethOn> new names! <@SethOn> I'll partake in this * SethOn is now known as Voltronite <@Voltronite> there we go	Stage	2013-02-07 22:57:15.770688+01
75	[TETN] Sethic: nothing manlier than leaning out of an american muscle car aiming a sawed off shotgun at another muscle car [Fuzzbot] Steeltown: then pulling over to eat at mcdonalds  [TETN] Sethic: supersize? Hell yea	bocker	2013-02-07 22:57:15.770688+01
76	[Team] Steeltown: 6 omnis got in i think [Team] Steeltown: was 17 and now 9 [Team] Drawn: you are worse to math than i am rocker [Team] Drawn: thats 9 ! [Team] Steeltown: its 8... [Team] Drawn: ok, we suck	bocker	2013-02-07 22:57:15.770688+01
77	Nahuatl: Those dogs are costly, I'll warn you now ;-/  Nodeadeye: and they poop  Nodeadeye: very annoying	bocker	2013-02-07 22:57:15.770688+01
78	[Luzengie]: I don't bother pvp'ing [Luzengie]: life is too short To [Luzengie]: shorter for the other guy hopefully!	bocker	2013-02-07 22:57:15.770688+01
79	To [Thedevil]: traders deserve it! [Thedevil]: was on my crat [Thedevil]: i was like WHO CAN HIT ME NOW WITH MY EVADE PERKS UP [Thedevil]: *splat*	bocker	2013-02-07 22:57:15.770688+01
80	[11:11] <Means> I would want Steven Hawking for sure.  [11:11] <ShadowGod> hawkings in a mech....  [11:11] <Means> saying "You can't perk me bitches!" in that automated voice	bocker	2013-02-07 22:57:15.770688+01
81	<Fuzzbot> [DR] Hypnotik: !clump Kyr'Ozch Crossbow - Type 3 (http://auno.org/ao/db.php?id=254600&ql=125) <Fuzzbot> [DR] Hypnotik: only in tell? <Fuzzbot> [TETN] Xzentoren: ehm <Fuzzbot> [TETN] Xzentoren: you pasted a weapon	Stage	2013-02-07 22:57:15.770688+01
82	Never take life seriously. Nobody gets out alive anyway.	bocker	2013-02-07 22:57:15.770688+01
83	Always remember you're unique, just like everyone else.	bocker	2013-02-07 22:57:15.770688+01
84	[Hellcom] Syyrgeon: all your raffles are belong to me :) [Hellcom] Entagain: Ban neuts	bocker	2013-02-07 22:57:15.770688+01
85	To [Supernaute]: 15m? [Supernaute]: yes To [Supernaute]: sure, bor grid? [Supernaute]: yes To [Supernaute]: you are a man of many words! [Supernaute]: yes	bocker	2013-02-07 22:57:15.770688+01
86	[Thepureone]: im not good at lerning	bocker	2013-02-07 22:57:15.770688+01
87	Computer games don't affect kids; I mean if Pac-Man affected us as kids, we'd all be running around in darkened rooms, munching magic pills and listening to repetitive electronic music.	bocker	2013-02-07 22:57:15.770688+01
88	You've got questions. We've got dancing paperclips.	bocker	2013-02-07 22:57:15.770688+01
89	Thus the metric system did not really catch on in the States, unless you count the increasing popularity of the nine-millimeter bullet.	bocker	2013-02-07 22:57:15.770688+01
90	Evolution is slow, small pox is fast.	bocker	2013-02-07 22:57:15.770688+01
91	Fuzzbot: Drakosmule2: well even if I had kissed a donkey I doubt it would of given me walking pneumonia / bronchitis Fuzzbot: Zerofate: depends on the donkey really	bocker	2013-02-07 22:57:15.770688+01
92	[Fuzzbot] Lolith: grey pb 1, 220 keeper 0	bocker	2013-02-07 22:57:15.770688+01
93	[Raid] Battlespork: My unholy fusion of irish, hungarian and austrian genes consider alcohol to be an acceptable substitute for water, or, in a pinch, oxygen	bocker	2013-02-07 22:57:15.770688+01
94	[Raid] Solidstriker: lol, if you want in, say 'IN' after this post [Raid] Fourthaid: OUT [Raid] Fourthaid: IN [Raid] Fourthaid: OUT [Raid] Fourthaid: IN	bocker	2013-02-07 22:57:15.770688+01
95	[Bleepbot] Lolith: 0223434344n455n	bocker	2013-02-07 22:57:15.770688+01
96	[Transchat] Biancha: i keep getting offline tells from the same guy asking if i speak romanian o.o	bocker	2013-02-07 22:57:15.770688+01
97	[Yusukia]: duel? [Yusukia]: duel? To [Yusukia]: go fight other lovechilds! [Yusukia]: takes to long	bocker	2013-02-07 22:57:15.770688+01
98	[Martho]: i got some lowbie with zero sentence forming skills fetching atrox pie for a wrangle	bocker	2013-02-07 22:57:15.770688+01
99	 [Biancha]: she's not used to stuf other than inferno missions i think [Fuzzbot] Fuzzbot: Zerofate: she sucks at inferno missions too	bocker	2013-02-07 22:57:15.770688+01
100	Biancha: i got a great waffle iron at the thrift store, can't wait to try it! Steeltown: you dont know where thats been. Biancha: i sanitized it! Steeltown: men will try to waffle anything when they are bored or horny. ANYTHING. Biancha: O_O Biancha: stop that lol	bocker	2013-02-07 22:57:15.770688+01
101	[Neu. Newbie OOC] Gogringogo: OMFG, I have made 7 diff charaters and evertime I start playing with it my shuttle goes down!! WTF!! what am I doing wrong?	bocker	2013-02-07 22:57:15.770688+01
102	 [Team] Betancore: kotts you went into a clothes shop [Team] Kotts: to sell loot.... honest...	bocker	2013-02-07 22:57:15.770688+01
103	Originally Posted by mrcreds: Sixximux forever	bocker	2013-02-07 22:57:15.770688+01
104	[Transchat] Fixrocker: prolly just accident [Transchat] Jaelacacia: pets gone wild? [Transchat] Fixrocker: the alcohol lowered the pets inhibitions [Transchat] Jaelacacia: bad pets	bocker	2013-02-07 22:57:15.770688+01
105	Luckyspawn: spartans know all about pvp in BS /macro sparta /s THIS IS... SPARTA!!! \\n /afk	bocker	2013-02-07 22:57:15.770688+01
106	 [Transchat] Biancha: i got invite to a mission!!!!!!! [Transchat] Biancha: but then they said they needed a bigger tank :( [Transchat] Transchat: [Transcendence] Solidstriker: challenger!	bocker	2013-02-07 22:57:15.770688+01
107	[Transchat] Biancha: what's a DP? [Transchat] Eseb: you don't want to know [Transchat] Biancha: probably true	bocker	2013-02-07 22:57:15.770688+01
108	X [06:29] Stage: Well, apparently several people in this channel know guys so whipped that their GFs buttfucked them [06:29] xzanThour: ... you dunno what yorue missing	[KP]	2013-02-07 22:57:15.770688+01
109	[Transcendence] Darkkblood: has anyone seen the movie "I love you man"? [Transchat] Transchat: Fourthaid: is that like a code?	bocker	2013-02-07 22:57:15.770688+01
110	[Transchat] Transchat: [Transcendence] Fourthaid: there is no such thing as a "wrong hole". [Transchat] Fixrocker: one of those holes they cut in your throat when you enjoy cigarretes too much [Transchat] Transchat: [Transcendence] Fourthaid: gives new meaning to "throat fuck"	bocker	2013-02-07 22:57:15.770688+01
153	Midway's closure brought an end to a series of increasingly unfunny "Finish him!" puns.	bocker	2013-02-07 22:57:15.770688+01
192	Bus you totally screwed that up	[KP]	2013-02-07 22:57:15.770688+01
111	[TETN] Bockrocker: you need apf/tnh belts for any serious twinking anyway [TETN] Tetnbot: [DR] Byracka: And not everyone needs to twink to your standards to be content Rocker [TETN] Bockrocker: 300 or bust!!! [TETN] Grittlez: that's what the spartans said, look at them now	bocker	2013-02-07 22:57:15.770688+01
112	A South Carolina man was arrested for having sex with a horse. And he would have gotten away with it too, if the horse hadn't bragged about it on Facebook.	bocker	2013-02-07 22:57:15.770688+01
113	Snur: tho i did make a penis joke to aeth- i remarked at how big he [Statue of David] was......and that the rest of him is pretty big too, then he scowled at me	bocker	2013-02-07 22:57:15.770688+01
114	Martho: [Dronki]: like a tip? [Dronki]: 200k ok? To [Dronki]: do the teapot song in vicinity, thanks Martho: Dronki shouts: I'm a little teapot short and stout	bocker	2013-02-07 22:57:15.770688+01
115	Zavy [04:12] minizav: im a polite girl... i dont say no	[KP]	2013-02-07 22:57:15.770688+01
116	Bus [17:36:21] Kharse > I actually have long hair & got jugs!	[KP]	2013-02-07 22:57:15.770688+01
117	Dagenham: NV pretty much degenerated into "A place where people log on, log off, and ask me to tradeskill when I'm on BS".	bocker	2013-02-07 22:57:15.770688+01
118	Mox <Mox|lurksalot> k, that's it, logging done. now pitches a tent for Zav - I need to have a chat.	[KP]	2013-02-07 22:57:15.770688+01
119	[Fuzzbot] Ministorage: do the danish have a word for baconaise [Fuzzbot] Fuzzbot: [IRC] scheurel: youre just making stuff up now	bocker	2013-02-07 22:57:15.770688+01
120	To [Martho]: but um...arent all the towers you can make from shop buyable parts, buyable premade? Martho: but i sell certain nice qls! Martho: that are not always in shops To [Martho]: like ql200, aka the srompu special	bocker	2013-02-07 22:57:15.770688+01
121	[Fuzzbot] Marthnom: but hey, we've played ao for long time. they could just shove ice picks through our skulls and we'd ask do we get phats now	bocker	2013-02-07 22:57:15.770688+01
122	Zavy Zavi: i wish i had some dishes to do :P	[KP]	2013-02-07 22:57:15.770688+01
123	 [Losthopebot] Alcoydel: i have infraction for using phrase "Klingon bastards" which apparently was a racist comment	bocker	2013-02-07 22:57:15.770688+01
124	[Fuzzbot] Fixrocker: [Jonax]: You have two of the same unique item on one character? [Jonax]: You do not remember the exact name at all? [Jonax]: And you don't know how you accomplished this? [Fuzzbot] Fixrocker: wtf. who is the gm here [Fuzzbot] Fixrocker: you tell me how i did it	bocker	2013-02-07 22:57:15.770688+01
125	[Fuzzbot] Fuzzbot: [IRC] Zavy: its doesnt make sense [Fuzzbot] Fuzzbot: [IRC] Zavy: 1hb, you can have 2 weapons and 2hb you can have one [Fuzzbot] Fuzzbot: [IRC] Zavy: see why i get confused?	bocker	2013-02-07 22:57:15.770688+01
126	[IRC] SethOn: like there's no "o" in "djeooolooohe" according to danes	bocker	2013-02-07 22:57:15.770688+01
127	[Fuzzbot] Fixrocker: whats lucky star [Fuzzbot] Fuzzbot: Zerodestiny: anime youtube it! [Fuzzbot] Fuzzbot: Zerofate: anime is awesome :D [Fuzzbot] Fixrocker: on a scale of 1 to 10 tentacles, how awesome is it	bocker	2013-02-07 22:57:15.770688+01
128	[IRC] Zavy: there are so many similar words in danish, german and dutch, you of all should be able to understand a bit! [Fuzzbot] Steeltown: like i say tomato, you say tomat??	bocker	2013-02-07 22:57:15.770688+01
129	[Team] Zerofate: most poop is solid [Team] Martho: pics or didnt happen	bocker	2013-02-07 22:57:15.770688+01
130	[Team] Biancha: i only have gimpy mongo still too ^^ Team] Biancha: and Olol was like ARG BACKSTAB LOL ARG KILL DD etc.	bocker	2013-02-07 22:57:15.770688+01
131	To [Homilla]: make a montage of your fixer dying to triple in various locations around rk [Homilla]: done that	bocker	2013-02-07 22:57:15.770688+01
132	[Transcendence] Fixrocker: so...wut you doin solid [Transcendence] Markerz: idling in high rise 3 [Transcendence] Markerz: u?! [Transcendence] Fixrocker: idling outside high rise 3	bocker	2013-02-07 22:57:15.770688+01
133	[Transcendence] Fixrocker: lucvky on eof omoe sob e er soweorkers wvoluntwerr to drive rockerhomer [Contributed by: Speedyadvy]	bocker	2013-02-07 22:57:15.770688+01
134	[Fuzzbot] Fuzzbot: Zerodestiny: thats like me having kids [Fuzzbot] Fuzzbot: Zerodestiny: id cosplay them	bocker	2013-02-07 22:57:15.770688+01
135	[Fuzzbot] Fuzzbot: [IRC] Livewirez: and the person with balls does the thinking [Fuzzbot] Fuzzbot: [IRC] Livewirez: in your case - not good [Fuzzbot] Fuzzbot: [IRC] Zavy: lalalalallallalalalalalala	bocker	2013-02-07 22:57:15.770688+01
136	[Safety Not Guaranteed] Darkellarisa: someone kill xyphos irl please -_- Alcoydel: if i met him i'd honestly just  chestkick him while yelling THIS...IS...WORKSHOP!	bocker	2013-02-07 22:57:15.770688+01
137	[Losthopebot] Haruuko: you all remind me of grumpy 80 year old men complaining about "kids these days" and "why isnt bread still 10 cents a loaf"	bocker	2013-02-07 22:57:15.770688+01
138	Fuzzbot: [IRC] [KP]: well I hope you took lots of pics, so you can make an abrasively sarcastic page about what a great time you had Fuzzbot: [IRC] Zav_afk: no pics, just awful memories	bocker	2013-02-07 22:57:15.770688+01
139	[Safety Not Guaranteed] Crazyella: bank transfer takes 2 days [Losthopebot] Esthaer: 2 days? you have to rob it forst?	bocker	2013-02-07 22:57:15.770688+01
140	[Transcendence] Fourthaid: snakes in a cage [Transcendence] Fourthaid: very boring flick	bocker	2013-02-07 22:57:15.770688+01
141	It was inspiring to see white men finally stand up to the oppressive, rigged system that has forced them to live in a hopeless cycle of wealth and opportunity.	bocker	2013-02-07 22:57:15.770688+01
142	This is our last best shot for a long time to get the sort of serious healthcare reform that would make the United States the envy of several African nations.	bocker	2013-02-07 22:57:15.770688+01
143	[Safety Not Guaranteed] Hellarisa: on monday we know who all is nominated for engi pro :) [Losthopebot] Lordstage: I think I better nominate myself, I bet nobody else has :p [Losthopebot] Lordstage: My campaign slogon is "It's me or the bucket"	bocker	2013-02-07 22:57:15.770688+01
144	Biancha: at a party i threw i made tofu wrapped in bacon for a snack  Biancha: it offended everyone equally	bocker	2013-02-07 22:57:15.770688+01
145	[Losthopebot] Takun: wow I'm pro [Losthopebot] Takun: forgot to use cocoon [Losthopebot] Takun: pvp is hard D: [Losthopebot] Fixrocker: did you remember to fling tho [Losthopebot] Takun: yes sir	bocker	2013-02-07 22:57:15.770688+01
146	[Ninjatroxx]: ql300 kyr wep + biomat To [Ninjatroxx]: think it needs 1800 WS, don't have it sorry [Ninjatroxx]: hmm can i pay u to have that?	bocker	2013-02-07 22:57:15.770688+01
147	[Zaani]: moaning tigers is so sexy To [Zaani]: :D [Zaani]: like.. rawwrrrr rawwrr take me rawwrrr	bocker	2013-02-07 22:57:15.770688+01
148	[Raid] Corbeex: in my book, this is nice pull :D [Raid] Rodrodyndron: should be written by atrox 8P	bocker	2013-02-07 22:57:15.770688+01
149	[Raid] Fixrocker: we almost died like 10 times rod! [Raid] Rodrodyndron: thanks, and you are lier [Raid] Fixrocker: :(	bocker	2013-02-07 22:57:15.770688+01
150	[Team] Biancha: the hardest thing to tank ever is an iraid zerg [Team] Biancha: each doc counts for 20% of 1 normal doc	bocker	2013-02-07 22:57:15.770688+01
151	 [Losthopebot] Thedevil: 200k for 1 [crystal filled with source] on gms [Losthopebot] Thedevil: might make more money with dancing in bor	bocker	2013-02-07 22:57:15.770688+01
191	[IRC] Stage: Probably along the lines of: A wild japanese girl appears! Zero uses Sake! It's not very effective...	bocker	2013-02-07 22:57:15.770688+01
154	Fuzzbot: |KP|: god stop with your sake shit Fuzzbot:  |KP|: it's horrid :P  Fuzzbot: |KP|: drinking it is not going to get you to score with a japanese cartoon girl [Fuzzbot] Fuzzbot: Zerodestiny: sad moment	bocker	2013-02-07 22:57:15.770688+01
155	[Fuzzbot] Fuzzbot: Livewirez: iraid is awsome, EVERY doc fence and the tank who is the RL fenecd too. priceless	bocker	2013-02-07 22:57:15.770688+01
156	[Fuzzbot] Fuzzbot: [IRC] Katyri: llama has 60 games! [Fuzzbot] Fuzzbot: [IRC] Katyri: what are you? scrooge mcduck of games ?	bocker	2013-02-07 22:57:15.770688+01
157	<[minizav]> I can hardly imagine that i ever said anything studid and worth quoting	Stage	2013-02-07 22:57:15.770688+01
158	[Fuzzbot] Fuzzbot: [IRC] scheurel: theyre trying to take over couch [Fuzzbot] Fuzzbot: [TETN] Laaki: dunno about taking over couch, last time I was here I saw a tumbleweed rolling past [Fuzzbot] Fuzzbot: [IRC] scheurel: GERMAN TUMBLEWEED!	bocker	2013-02-07 22:57:15.770688+01
159	[Raid] Regenworm: how would you type the sound that a bat makes :O [Raid] Steeltown: scree scree [Raid] Whitelights: splay? [Raid] Bachir: squiggle [Raid] Livewirez: fap fap fap XD	bocker	2013-02-07 22:57:15.770688+01
160	Zavy [IRC] iZav: Omfg they dragged me into This bimbo place! Justonefix: clearly not the place you belong to! iZav: Compromised, gotta go	[KP]	2013-02-07 22:57:15.770688+01
161	[Transcendence] Fixrocker: i was playing in bs earlier and i sent some new german engy a tell saying something like "you have an annoying amount of survivability!" [Transcendence] Fixrocker: he sent me back a message in german, i had sferykal translate it, he said "i don't know what you are saying, but to be safe i am ignoring you"	bocker	2013-02-07 22:57:15.770688+01
162	[Losthopebot] Alcoydel: but yeah full alphas can be so convenient Alcoydel: or rather, less inconvienient that not having full alphas Alcoydel: i sincerely apologise for using convenient in engi related sentence	bocker	2013-02-07 22:57:15.770688+01
163	A report released Monday by the U.S. Department of Commerce revealed that Americans spend an astonishing $14 trillion a year on countless, usually failed attempts to look cool.	bocker	2013-02-07 22:57:15.770688+01
164	Djantro shouts: udel any1 ? Zidane shouts: udel dudel? :o Aasimi shouts: strudel?	bocker	2013-02-07 22:57:15.770688+01
165	[Losthopebot] Alcoydel: When the sun goes into supernova and earth will evacuate, the last of the engineers will still be here, trying to buff	bocker	2013-02-07 22:57:15.770688+01
166	[Fuzzbot] Livewirez: i had a dude ask me "can i put my baby in the sink" im like "yeah but... why?"	bocker	2013-02-07 22:57:15.770688+01
167	[Fuzzbot] Livewirez: we changed the national anthem twice so to be onthe safer side we know both [Fuzzbot] Livewirez: couse none remembers which ones is the right one atm [Fuzzbot] Fixrocker: changing twice = 3 anthems! [Fuzzbot] Livewirez: but becuse of that our math skills suffered	bocker	2013-02-07 22:57:15.770688+01
168	Yeah well Bruce looks like a guy who just molested 10 kids in 5 minutes	bocker	2013-02-07 22:57:15.770688+01
169	[Fuzzbot] Fixrocker: 30+ year olds are supposed to be immune to vampireporn zav [Fuzzbot] Fuzzbot: [IRC] minizav: they are?	bocker	2013-02-07 22:57:15.770688+01
170	[Transcendence] Gentlycaress: sometimes i just charm mobs and run over to the person with the least hp and chase them around because they think they are being aggroed	bocker	2013-02-07 22:57:15.770688+01
171	Traderocker: your cloth tab is like half empty lol Vorga: lies i r very twinked	bocker	2013-02-07 22:57:15.770688+01
172	[Brampfine]: a proposal to proposal a proposal for a proposal to send a proposal for a proposal [Brampfine]: ah bad translate [Brampfine]: apply	bocker	2013-02-07 22:57:15.770688+01
173	[Team] Rdord: this tank is kinda annoying [Team] Mamimii: that tank is my boyfriend thank you very much	bocker	2013-02-07 22:57:15.770688+01
174	[Fuzzbot] Fuzzbot: [IRC] |KP|: what the hell is a remote link for furniture? [Fuzzbot] Fuzzbot: [IRC] |KP|: it SMSs you when it needs re-stuffing or something?	bocker	2013-02-07 22:57:15.770688+01
175	[Team] Ambivalencia: these pictures are getting a bit homoerotic zero [Team] Zeroparts: you just dont know what your looking at :P [Team] Ambivalencia: it appears to be a mech with a giant sword for a penis.	bocker	2013-02-07 22:57:15.770688+01
176	 [Fuzzbot] Traderocker: stage logged on and immediately ran off to do it with normies [Fuzzbot] Fuzzbot: [TETN] Quintrell: normies?  [Fuzzbot] Traderocker: people he met outside of the approved channels [Fuzzbot] Fuzzbot: [TETN] Quintrell: "approved channels", haha  [Fuzzbot] Traderocker: those channels being fuzzbot and transchat and maybe losthopebot	bocker	2013-02-07 22:57:15.770688+01
177	[Transcendence] Transchat: Markerz, Terrible is a fictional Marvel comics character. [Transcendence] Transchat: [Guest] Battlespork: I'm sure he lived up to his name	bocker	2013-02-07 22:57:15.770688+01
178	To [Bitbucket]: shiny first, it is just high enough to fit.  need it to be at least 297 or so with both i think, can probably get it to 300 myself with the faded [Bitbucket]: shiny went in first, but I bumped it too high :( To [Bitbucket]: dude you put the bright in first lol [Bitbucket]: aw...  nvm I did it backwards [Bitbucket]: shit sorry	bocker	2013-02-07 22:57:15.770688+01
179	To [Matakar]: why am i healing :( [Matakar]: lol no idea [Team] Matakar: heal u there m8? [Team] Healbith: u am healing	bocker	2013-02-07 22:57:15.770688+01
180	[Strupstad]: I've been at their (FC's) office. they have 5 on game development and 10 on updating their facebook page	bocker	2013-02-07 22:57:15.770688+01
181	[Raid] Biancha: i used to be too shy but now i drink more	bocker	2013-02-07 22:57:15.770688+01
182	To [means]: can has waffling insta 200! means: maybe someday...but not at the top of our list right now To [means]: whatcha doing anyway, some party at the grind or something!? means: We are working on the things in your signature on the forums	bocker	2013-02-07 22:57:15.770688+01
183	[Transchat] Ministorage: cheech marin plays pencil [Transchat] Transchat: [Transcendence] Pencilpirate: i demand tom selleck  [Transchat] Transchat: [Transcendence] Pencilpirate: or someone with a moustache of equal or greater power	bocker	2013-02-07 22:57:15.770688+01
184	[Fuzzbot] Ministorage: so um...tell us why your family is named after boats {Fuzzbot] Fuzzbot: [IRC] iZav: rocker, because they liked boats	bocker	2013-02-07 22:57:15.770688+01
185	[Fuzzbot] Fuzzbot: [IRC] Ll4m4: I found a kite team in pets vs monsters	bocker	2013-02-07 22:57:15.770688+01
186	[Fuzzbot] Fuzzbot: [IRC] [KP]: agents mimicing engi often has CH, right [Fuzzbot] Ministorage: ...wut? [Fuzzbot] Fuzzbot: Lordstage: wat	bocker	2013-02-07 22:57:15.770688+01
187	[Transchat] Transchat: [Transcendence] Marthnom: moonis is from west coast [Transchat] Transchat: [Transcendence] Marthnom: straight up, yo [Transchat] Tradette: i am! [Transchat] Tradette: in finland tho [Transchat] Transchat: [Transcendence] Marthnom: yea finland doesnt really count, we only have west coast [Transchat] Tradette: you said it, not me	bocker	2013-02-07 22:57:15.770688+01
188	Biancha: i killed my facebook and haven't made another yet Biancha: and not sure i want my real friends to know how nerdy i am lol	bocker	2013-02-07 22:57:15.770688+01
189	[IRC] Stage: All bow to our mighty bacteria overlords	bocker	2013-02-07 22:57:15.770688+01
190	[Fuzzbot] Fuzzbot: [IRC] Stage: But they have the most retarded lion on their coat of arms [Fuzzbot] Fuzzbot: [IRC] Martho: you have an emu and kangaroo hugging, look whos talking	bocker	2013-02-07 22:57:15.770688+01
193	[Fuzzbot] Martho: its like the fat from donuts engulfs all the healthy things you eat and then body goes "oh its pure fat let it through" and you poop all the healthy stuff out too	[Stage]	2013-02-07 22:57:15.770688+01
194	[IRC] |KP|: norwegian cuisine sounds more like a viking "what's left on the boat?" thing [IRC] |KP|: "oh well there's some rotting fish here" [IRC] |KP|: "sweet!"	[Stage]	2013-02-07 22:57:15.770688+01
195	[TETN] Zavy: i've got other brilliant skills instead of lame mapreading :P	[Stage]	2013-02-07 22:57:15.770688+01
196	[IRC] |KP|: uh, zav is pristine and virginal, and was brought to her parents by the stork, apparently	[Stage]	2013-02-07 22:57:15.770688+01
197	[IRC] |KP|: if zero was zavys kid, she'd be into gay porn	[Stage]	2013-02-07 22:57:15.770688+01
198	Why not quote zoidberg? (V)(;,,;)(V)	[Stage]	2013-02-07 22:57:15.770688+01
199	Ministorage: i has no frainds online Ministorage: riv is on ;p	[Stage]	2013-02-07 22:57:15.770688+01
200	<Busata> and killing someone with an arrow isn't exactly violent	[Stage]	2013-02-07 22:57:15.770688+01
201	Zendadaist: it means my current boss no longer has me as his underling Zendadaist: he actually refers to me as that btw	[Stage]	2013-02-07 22:57:15.770688+01
202	<kikkerpreut> you can dial with lips i bet <kikkerpreut> wheres my phone i need to try this <kikkerpreut> ... lips dont work, tongue does tho	[Stage]	2013-02-07 22:57:15.770688+01
203	<Busata> I *might* be your style <kikkerpreut> youre not, need more boobs	[Stage]	2013-02-07 22:57:15.770688+01
204	[14:21] Stage: Cat needs watering!	[Stage]	2013-02-07 22:57:15.770688+01
206	One Dozen Stuffed Felt Mustaches on a Stick	bocker	2013-02-07 22:57:15.770688+01
207	<+[Stage]> Now you broke my ability to spell "license" <+[Stage]> gj <Joeymtl> i corrupted your grammar <Joeymtl> you've been frenched <Joeymtl> wait.. <Joeymtl> no	[Stage]	2013-02-07 22:57:15.770688+01
208	this is quote 208	kikkerpreut	2013-02-07 22:57:15.770688+01
209	this is not quote 208	kikkerpreut	2013-02-07 22:57:15.770688+01
210	<busata> isn't this game amazing... <busata> "press prone to prone"	[Stage]	2013-02-07 22:57:15.770688+01
211	<@[Stage]> I'll help you in moon bog if you stop trying to get me to molest your anus	bocker	2013-02-07 22:57:15.770688+01
212	<@|Stage|> but you also become more manly as you become less hairy  <busata> said no one ever!	bocker	2013-02-07 22:57:15.770688+01
213	 <[KP]> Bus, fix your nipples	[Stage]	2013-02-07 22:57:15.770688+01
214	<bocker> here in america we go for a leisurely bugger all the time	[Stage]	2013-02-07 22:57:15.770688+01
215	[DR] Zarbie: ye, i have moved the nuke to ubt spot! [DR] Zarbie: maybe thats better for me! [DR][Guest] Traderocker: what happens when you get 10 nukes! [DR] Zarbie: i die!	[Stage]	2013-02-07 22:57:15.770688+01
216	[2006] Fixrocker: hmmm on lft for 215+ research and invited to team by a 198 adv in pred and ancient container...so conflicted	[Stage]	2013-02-07 22:57:15.770688+01
217	Traderocker: i dont get it, someone bought the one at 400 and all the others are still there? Ihnnaw: checked side differences of the shops? Traderocker: i dont know what side "thats hot" is lol	[Stage]	2013-02-07 22:57:15.770688+01
218	Samanthafix: Eew, drama. I'll stick my head in the sand and pretend it isn't happening Grimzone: i'm just gonna sit on my hands and bang my head on the keyboard till it's over Grimzone: dlkdjdglepoa'lkgf	[Stage]	2013-02-07 22:57:15.770688+01
219	Fixrocker: You tried to hit That is teh ticklez, but missed! Fixrocker: ??? Kapirexa: gimp :P Ihnnaw: GM changed name of it :P Fixrocker: oh his name changed Fixrocker: You hit Fixrocker can't touchz mehz for 2491 points of energy damage.Critical hit! Fixrocker: :(	[Stage]	2013-02-07 22:57:15.770688+01
220	 Moxet: daghters; ghods punishment for being a man Ihnnaw: you rather wanted boys Mox? Moxet: I wanted a gemeral anaesthetic :P	[Stage]	2013-02-07 22:57:15.770688+01
221	[Team] Elikko: I can imagine kp as a ship capitan... "Yarr! We need more leaks, this is too easy. Ensign, get me a drill!"	[Stage]	2013-02-07 22:57:15.770688+01
222	[Team] Drakodeventr: join the club of crap	[Stage]	2013-02-07 22:57:15.770688+01
223	[Neu. OOC] Martiques: How much does a 131 wrangle raise your skills?	[Stage]	2013-02-07 22:57:15.770688+01
224	[Clan OOC] Mathodical: 47 NT lft!, i wont get u killed!	[Stage]	2013-02-07 22:57:15.770688+01
225	[Team] Battlespork: The spider running up my leg made that wave extra exciting.	[Stage]	2013-02-07 22:57:15.770688+01
226	Yllkoda: how many are really needed for the Beast? Ihnnaw: 3-4 teams Katyri: like .... 20	[Stage]	2013-02-07 22:57:15.770688+01
227	<Busata_>:\tif my attic would have been already renovated like in my dreams, I'd let you sleep there! <kikkerpreut>: and let me watch you sleep at night <Busata_>: offer retracted	Bockrocker	2013-02-07 22:57:15.770688+01
228	[DR]   Lordstage: Are doc procs any good anyway? [DR]   Fixrocker: team init debuff is uber	Bockrocker	2013-02-07 22:57:15.770688+01
229	[DR]   Laymille: heh, sell your phats on ebay! [DR]   Laymille: oh wait... you deleted em [DR]   Ihnnaw: -.-	Bockrocker	2013-02-07 22:57:15.770688+01
230	[DR]   Fixrocker: [Wolf Brigade] Packnet: [Divine Retribution][Guest] Fixrocker: [Wolf Brigade] Packnet: [Divine Retribution][Guest] Fixrocker: i can see myself talk and stuff  [DR]   Fixrocker: the never ending headers!!!!	Bockrocker	2013-02-07 22:57:15.770688+01
231	[DR]   Holysin: [Ilyacze]: hi [Ilyacze]: sel [Ilyacze]: sell To [Ilyacze]: yes? [Ilyacze]: Basic Vest Basic Cloak Basic Headwear	[Stage]	2013-02-07 22:57:15.770688+01
232	[DR]   Lolith: sawa died so fast he had two bodies	[Stage]	2013-02-07 22:57:15.770688+01
233	Holysin: [Team] Holysin: are you assisting me? [Team] Ipnose2: yes Holysin: 4 minutes later Holysin: [Team] Ipnose2: i need create macrro assist but i don't know how [Team] Ipnose2: can help plz Poisonwood: lol Sux to be you	|Stage|	2013-02-07 22:57:15.770688+01
234	Bloodforbaal: Where the hell did all these TL4 toons come from? o.O Rodrodyndron: from TL3 ;)	[Stage]	2013-02-07 22:57:15.770688+01
235	Bornslippy: "It usually means that a gang of sex ducks are making giant bendy penises frolic around to the tune of ?Dance of the Sugar Plum Fairy.? "	[Stage]	2013-02-07 22:57:15.770688+01
236	Bornslippy: heh, yuo have to buy genitals	[Stage]	2013-02-07 22:57:15.770688+01
237	Traderocker: i wear 200m worth of stuff on my butt!	[Stage]	2013-02-07 22:57:15.770688+01
238	[March 2007] Holysin: darkman should apply for leader :p Ihnnaw: doubt he'll get it :P	[Stage]	2013-02-07 22:57:15.770688+01
239	Ceirwyn: so, a quick threesome? ^^	[Stage]	2013-02-07 22:57:15.770688+01
240	[DR]   Fixrocker: heart is annoying  [DR]   Lordstage: You don't say.  [DR]   Ihnnaw: o rly?  [DR]   Kaboombastik: actually making him tank is a nice trick to make him stfu a bit	Bockrocker	2013-02-07 22:57:15.770688+01
241	[DR]   Traderocker: whats second life? is that where i go when im not in ao?  [DR]   Lordstage: No, it's a kinda a MMO...	Bockrocker	2013-02-07 22:57:15.770688+01
242	[DR]   Lordstage: [Shotgunsally]: hey [Shotgunsally]: hello [Shotgunsally]: u been a engi long?	Bockrocker	2013-02-07 22:57:15.770688+01
243	Adminzero: locl ,e please im rrunkn and dont want to mae ans ass of self	[Stage]	2013-02-07 22:57:15.770688+01
244	Layertrox shouts: You hit Bloodforbaal for 2 points of Sneak Atck damage.	[Stage]	2013-02-07 22:57:15.770688+01
245	[DR]   Sintered: [OT OOC] Demonhunter3: can you get exp from pvp? [OT OOC] Rockytrooper: nope [OT OOC] Demonhunter3: oh [OT OOC] Demonhunter3: what is the pont of pvp then?	Bockrocker	2013-02-07 22:57:15.770688+01
246	[DR]   Lordstage: Berael thought it'd be a good idea if our bots weren't linked any more.	Bockrocker	2013-02-07 22:57:15.770688+01
247	[DR]   Lordstage: Lordstage: We aren't WB Mk. 2 Drizztt: No your DR the elite'ists	Bockrocker	2013-02-07 22:57:15.770688+01
248	[DR]   Fixrocker: [Chimx]: get NR you noob To [Chimx]: i have 2600 nr, it doesnt work! [Chimx]: perk NR To [Chimx]: i DID, thats how i got 2600	Bockrocker	2013-02-07 22:57:15.770688+01
249	[DR]   Fixrocker: i am free of teh NR!  [DR]   Lordstage: What was it like? :P  [DR]   Fixrocker: i was only llamas bitch for 50% of the time	Bockrocker	2013-02-07 22:57:15.770688+01
250	[DR]   Kaboombastik: ./shout all look Fixrocker he's the ugliest fixer of all AO after Appleface	Bockrocker	2013-02-07 22:57:15.770688+01
251	[DR]   Acidlogic: he's a bloody awful tank  [DR]   Acidlogic: if you do all the work of aiming him at something he's ok at hitting FA/burst	Bockrocker	2013-02-07 22:57:15.770688+01
252	[DR]   Ihnnaw: [Hellcom] Adventftw: lol..howd we get loot [Hellcom] Ihnnaw: not assisting? [Hellcom] Adventftw: yea thats it	Bockrocker	2013-02-07 22:57:15.770688+01
253	Bockrocker\ti still question the necessity of reviving a language that has passed  [Stage]\tThen this giant israeli guy would probably punch you on the nose	Bockrocker	2013-02-07 22:57:15.770688+01
254	<[Stage]> Apparently it's impossible to talk about political matters in javanese <Bockrocker> i wish for this feature in english	[Stage]	2013-02-07 22:57:15.770688+01
255	Quilluck\tI'm going dwarf mermaid	Bockrocker	2013-02-07 22:57:15.770688+01
256	[KP]\tbus will play anything that runs in front & gets shot in the back  [KP] that's definitely bus [KP]  just about lined up, and "RARHARHARHARHARINTHEWAY"	Bockrocker	2013-02-07 22:57:15.770688+01
257	<Lazy> this anime has too much drama and lesbians and not enough mahjong <Lazy> i'm starting to lose interest	Stage_	2013-02-07 22:57:15.770688+01
258	<vespah> Asians are weird like that	_Stage_	2013-02-07 22:57:15.770688+01
259	[13:30] <@[Stage]> But it takes fucking *tons* of goats	Busata	2013-02-07 22:57:15.770688+01
260	<Lazy> I buy all my guns from T-Rex. He's a small arms dealer.	Stage__	2013-02-07 22:57:15.770688+01
261	<[Stage]> People often get decapitated when I zone <[Stage]> And sometimes their heads go halfway through the chat window <vhab> it's uhm, feature yes <vhab> dynamic head-chat-window interaction. it's cutting edge technology	[Stage]	2013-02-07 22:57:15.770688+01
262	<Ophi> Fisting people is how Nightmares are run.	[Stage]	2013-02-07 22:57:15.770688+01
263	(Bockrocker) 3 game directors have lived and died in the shadow of the engine. lets see if we can get to double digits	Quilluck	2013-02-07 22:57:15.770688+01
264	After the sewage gets down to the holding tank it goes to a treatment tank via a macerator pump aka "turd grinder" 	Bockrocker	2013-02-07 22:57:15.770688+01
265	<Darkempire> ass rape, not analysis rape 	Stage__	2013-02-07 22:57:15.770688+01
266	 [DR]  Lordstage: [link]Combined Sharpshooter's Sleeves[/link] just one piece to go :-) [DR]   Ihnnaw: gonna upgrade that? [DR]   Lordstage: Into what? GM armour? o.O	Bockrocker	2013-02-07 22:57:15.770688+01
267	[DR]   Lordstage: If I'm paying for levels I want someone to be running around in circles.	Bockrocker	2013-02-07 22:57:15.770688+01
268	[DR]   Moxet: stop there.... [DR]   Ihnnaw: what? your daughters reading along over your shoulders or something? :P [DR]   Byracka: They have to learn about people like Ihnn sooner or later! [DR]   Ihnnaw: \\o/	Bockrocker	2013-02-07 22:57:15.770688+01
269	[DR]   Moxet: what's considered the best doc armour - scots?  [DR]   Byracka: Well looking at Ihnn, I'd say randomly select six pieces of combined.	Bockrocker	2013-02-07 22:57:15.770688+01
270	[DR]   Fixrocker: the shade was like 200hp from dying, i was so happy. but it didnt pan out [DR]   Lordstage: Haha	Bockrocker	2013-02-07 22:57:15.770688+01
272	Unfortunately this notice is not an isolated incident. In another DMCA notice Microsoft asked Google to remove a Spotify.com URL and on several occasions they even asked Google to censor their own search engine Bing.	_Stage_	2013-02-07 22:57:15.770688+01
273	Egyptian slaves are a-okay! (in the olden days)	Seth_Box	2013-02-07 22:57:15.770688+01
274	Stage:\tLittle kids are creepy enough when they're normal	Bockrocker	2013-02-07 22:57:15.770688+01
275	<Llamatron> so inside a convo about shrooms and acid I got highlighted because of baguette, I am sad	Stage__	2013-02-07 22:57:15.770688+01
276	<Llamatron_> carefull if you go outside ophi, there's rumours of a wild beast roaming london streets <Ophi> Who? <Stage__> A grizzled, crazed-eyed and ancient beast, waving a cane and muttering about pills for its back! <Ophi> Oh god, who let Mox in?	Stage__	2013-02-07 22:57:15.770688+01
277	[DR]   Fixrocker: its your natural instinct to delete nodrops  [DR]   Ihnnaw: -.-	Bockrocker	2013-02-07 22:57:15.770688+01
278	[General] [*SpecialForce]: my first char was dragon, but i had some translation problems they talked chinese or whatever, so i changed to templer	[KP]	2013-02-07 22:57:15.770688+01
279	[KP]\theh time to find stage some revolting cocks	Bockrocker	2013-02-07 22:57:15.770688+01
280	StageSoAngry\tIf you start with the assumption "people are dumb" and go looking for evidence it quickly becomes impossible to understand how anything smart has ever been done	Bockrocker	2013-02-07 22:57:15.770688+01
281	<Bockrocker> book?  <Llamatron> it's some sheet of papers filled with words and put together, it's probably illegal in your country rocker:-(	Bockrocker	2013-02-07 22:57:15.770688+01
282	<|KP|> hmm, you could swing her around by her feet & use her chin to break rocks with <Bockrocker> i suppose thats one thing you can do with a girl	[Stage]	2013-02-07 22:57:15.770688+01
283	[13:23] StageSoAngry: This computer is bronking my lerngig	[KP]	2013-02-07 22:57:15.770688+01
284	<Bockrocker> that appears to be the legal limit of sideboob <Bockrocker> any more and its just boob	|Stage|	2013-02-07 22:57:15.770688+01
285	[*Abeadan|Game Master]: I'm glad I could help you recover your item! Good luck in all your future adventures and marriage proposals!	[Stage]	2013-02-07 22:57:15.770688+01
286	<[KP]> will find out if that fixed it when I next log in, I guess <[KP]> er <[KP]> flush <[KP]> haha was logging in as I typed	[Stage]	2013-02-07 22:57:15.770688+01
287	[Stage]\tNo cocksucking done whatsoever	Bockrocker	2013-02-07 22:57:15.770688+01
288	<Bockrocker> for a minute i thought you called yourself pugging filth <Bockrocker> taking your gaming to new heights of self loathing	[Stage]	2013-02-07 22:57:15.770688+01
289	<Bockrocker> lemme know when a player gives a referee a piledriver and grabs the mic to announce his feud with another player	[Stage]	2013-02-07 22:57:15.770688+01
290	[Group] [Emalee]: 894 hit not enough! Antimony Ministrix - 4,864 total dps - 00:50 duration Emalee - 1,366 dps - 8% crit - 22% pen - 12% glance - 11% block	[Stage]	2013-02-07 22:57:15.770688+01
205	<busata> you've sucked every shred of fun you can get out of that game, you're only playing because there's crack cocaine in it! (tm) (stage) 	[Drako]	2013-02-07 22:57:15.770688+01
291	[Losthopebot] Lordstage: I'm not a huge fan of this professional without an active account business though [Losthopebot] Lordstage: He asked me to log into test for him because he isn't fuckin subscribed	bockrocker	2013-02-11 23:27:00.491357+01
292	[20:12:55] [Fuzzbot] Fuzzbot: [IRC] Stage: whut whut? Name like dries? Total towel name [20:13:11] [Fuzzbot] Fuzzbot: [IRC] Busata: ur a towel!	bockrocker	2013-02-11 23:27:35.777308+01
293	<Hamham> You mean you haven't seen my massive cock? I should show you.	stage	2013-02-15 11:27:02.15481+01
294	<Nubs_> i had pug anxiety when i first started playing online games, but then i found out that everyone else i get teamed with with is also bad :P	stage	2013-02-17 10:34:24.12724+01
295	<Nubs> hmm i fell into a spike pit while in anima form	stage	2013-02-19 17:25:14.900893+01
296	<Quilluck> [11:49] [Badger-Badger]: \\o/ Always happy to slather green goo on a shapely ass!	stage	2013-02-28 13:51:45.112463+01
297	<Nubs> but i like my RamTEK-maxCHINA-TAIWANSPEED2000 ram	stage	2013-02-28 21:25:59.001914+01
299	<Busata_> we're all ODC'ers here!	stage	2013-03-04 15:10:34.368329+01
300	<@[Stage]> You'll end up spending the same amount over 4 years, probably more, trying to keep your heap of shit capable of barely running anything02	drako	2013-03-04 15:21:04.293647+01
301	<Bockrocker> i dont remember the old man in the sea calling the fish a cocksucker	stage	2013-03-17 12:08:42.948708+01
303	[Fuzzbot] Fuzzbot: [IRC] Minxxy: Tell SOlid to stfu! [Fuzzbot] Ministorage: its not solids fault that fc sends developers to ask engies questions, then writes down our answers, then puts the answers in a box and throws it into niagra falls [Fuzzbot] Ministorage: also i believe alcoydels sanity and technogen's dignity were in the barrel with the answers	bockrocker	2013-03-18 09:07:15.718573+01
304	<[KP]> now you have me imagining a world where scientology was founded by Rolf Harris	stage	2013-03-23 17:28:32.20367+01
305	[KP]\t.calc sunshine+lollipops [13:15]\tnipplegate\tsunshine+lollipops = lollipops + sunshine	bockrocker	2013-03-25 20:12:31.979948+01
306	<busata> I'm named like cheese	stage	2013-03-29 12:42:29.281221+01
307	[Group] [Nuubs]: attempti4ng to eporemove dog fromm laptop	quilluck	2013-03-29 17:54:54.046015+01
308	<Hamham> Banana ketchup is NOT weird	stage	2013-03-30 18:59:04.749363+01
309	[Stage]\tGet on vent and make umphing noises at each other!	bockrocker	2013-04-01 01:22:51.289956+02
310	<NubDroid> Me tanking is pregnant a rector for austere <NubDroid> Good job Swype	stage	2013-04-12 02:28:55.83729+02
311	<Nubs> swiftkey fucks up everything that isn't normal typing too <Nubs> trying to type terminal commands will usually end up with "bananna: command not found" or something	stage	2013-04-18 09:09:48.395813+02
312	Nubs:\ti got punished for not chopping enough wood\tNubs: my punishment was to chop wood	bockrocker	2013-04-25 07:51:39.381446+02
\.


--
-- Name: Quotes_quote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nipplegate
--

SELECT pg_catalog.setval('"Quotes_quote_id_seq"', 312, true);


--
-- Data for Name: tsw_Characters; Type: TABLE DATA; Schema: public; Owner: nipplegate
--

COPY "tsw_Characters" (character_id, name, user_name) FROM stdin;
12	Veevi	busata
13	Fourthfrayed	quilluck
14	Fourthblade	quilluck
15	Ophiuchus	ophi
16	Khurkh	ophi
17	VanDisaster	kp
18	Annachronism	kp
19	Stage	stage
\.


--
-- Name: tsw_Characters_character_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nipplegate
--

SELECT pg_catalog.setval('"tsw_Characters_character_id_seq"', 19, true);


--
-- Data for Name: tsw_DungeonLockouts; Type: TABLE DATA; Schema: public; Owner: nipplegate
--

COPY "tsw_DungeonLockouts" (lockout_id, character_id, dungeon_id, locked_minutes, added_on) FROM stdin;
143	18	13	808	2013-03-08 04:29:12.984214
144	18	15	854	2013-03-08 04:29:27.300375
146	18	14	854	2013-03-08 04:30:13.037271
147	18	17	911	2013-03-08 04:30:19.950006
\.


--
-- Name: tsw_DungeonLockouts_lockout_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nipplegate
--

SELECT pg_catalog.setval('"tsw_DungeonLockouts_lockout_id_seq"', 147, true);


--
-- Data for Name: tsw_Dungeons; Type: TABLE DATA; Schema: public; Owner: nipplegate
--

COPY "tsw_Dungeons" (dungeon_id, name, abbreviation, lockout_time) FROM stdin;
13	Polaris	POL	960
15	The Darkness War	DW	960
16	Hell Fallen	HF	960
17	Hell Eternal	HE	960
18	The Slaughterhouse	SH	960
19	The Facility	FAC	960
20	The Ankh	ANKH	960
14	Hell Raised	HR	960
21	Tyler Freeborn	tf	4080
\.


--
-- Name: tsw_Dungeons_dungeon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nipplegate
--

SELECT pg_catalog.setval('"tsw_Dungeons_dungeon_id_seq"', 21, true);


--
-- Name: Quotes_pkey; Type: CONSTRAINT; Schema: public; Owner: nipplegate; Tablespace: 
--

ALTER TABLE ONLY "Quotes"
    ADD CONSTRAINT "Quotes_pkey" PRIMARY KEY (quote_id);


--
-- Name: tsw_Characters_pkey; Type: CONSTRAINT; Schema: public; Owner: nipplegate; Tablespace: 
--

ALTER TABLE ONLY "tsw_Characters"
    ADD CONSTRAINT "tsw_Characters_pkey" PRIMARY KEY (character_id);


--
-- Name: tsw_DungeonLockouts_pkey; Type: CONSTRAINT; Schema: public; Owner: nipplegate; Tablespace: 
--

ALTER TABLE ONLY "tsw_DungeonLockouts"
    ADD CONSTRAINT "tsw_DungeonLockouts_pkey" PRIMARY KEY (lockout_id);


--
-- Name: tsw_Dungeons_name_key; Type: CONSTRAINT; Schema: public; Owner: nipplegate; Tablespace: 
--

ALTER TABLE ONLY "tsw_Dungeons"
    ADD CONSTRAINT "tsw_Dungeons_name_key" UNIQUE (name);


--
-- Name: tsw_Dungeons_pkey; Type: CONSTRAINT; Schema: public; Owner: nipplegate; Tablespace: 
--

ALTER TABLE ONLY "tsw_Dungeons"
    ADD CONSTRAINT "tsw_Dungeons_pkey" PRIMARY KEY (dungeon_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: Quotes; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON TABLE "Quotes" FROM PUBLIC;
REVOKE ALL ON TABLE "Quotes" FROM nipplegate;
GRANT ALL ON TABLE "Quotes" TO nipplegate;
GRANT ALL ON TABLE "Quotes" TO postgres;


--
-- Name: Quotes_quote_id_seq; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON SEQUENCE "Quotes_quote_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "Quotes_quote_id_seq" FROM nipplegate;
GRANT ALL ON SEQUENCE "Quotes_quote_id_seq" TO nipplegate;
GRANT ALL ON SEQUENCE "Quotes_quote_id_seq" TO postgres;


--
-- Name: tsw_Characters; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON TABLE "tsw_Characters" FROM PUBLIC;
REVOKE ALL ON TABLE "tsw_Characters" FROM nipplegate;
GRANT ALL ON TABLE "tsw_Characters" TO nipplegate;
GRANT ALL ON TABLE "tsw_Characters" TO postgres;


--
-- Name: tsw_Characters_character_id_seq; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON SEQUENCE "tsw_Characters_character_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "tsw_Characters_character_id_seq" FROM nipplegate;
GRANT ALL ON SEQUENCE "tsw_Characters_character_id_seq" TO nipplegate;
GRANT ALL ON SEQUENCE "tsw_Characters_character_id_seq" TO postgres;


--
-- Name: tsw_DungeonLockouts; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON TABLE "tsw_DungeonLockouts" FROM PUBLIC;
REVOKE ALL ON TABLE "tsw_DungeonLockouts" FROM nipplegate;
GRANT ALL ON TABLE "tsw_DungeonLockouts" TO nipplegate;
GRANT ALL ON TABLE "tsw_DungeonLockouts" TO postgres;


--
-- Name: tsw_DungeonLockouts_lockout_id_seq; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON SEQUENCE "tsw_DungeonLockouts_lockout_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "tsw_DungeonLockouts_lockout_id_seq" FROM nipplegate;
GRANT ALL ON SEQUENCE "tsw_DungeonLockouts_lockout_id_seq" TO nipplegate;
GRANT ALL ON SEQUENCE "tsw_DungeonLockouts_lockout_id_seq" TO postgres;


--
-- Name: tsw_Dungeons; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON TABLE "tsw_Dungeons" FROM PUBLIC;
REVOKE ALL ON TABLE "tsw_Dungeons" FROM nipplegate;
GRANT ALL ON TABLE "tsw_Dungeons" TO nipplegate;
GRANT ALL ON TABLE "tsw_Dungeons" TO postgres;


--
-- Name: tsw_Dungeons_dungeon_id_seq; Type: ACL; Schema: public; Owner: nipplegate
--

REVOKE ALL ON SEQUENCE "tsw_Dungeons_dungeon_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "tsw_Dungeons_dungeon_id_seq" FROM nipplegate;
GRANT ALL ON SEQUENCE "tsw_Dungeons_dungeon_id_seq" TO nipplegate;
GRANT ALL ON SEQUENCE "tsw_Dungeons_dungeon_id_seq" TO postgres;


--
-- PostgreSQL database dump complete
--

