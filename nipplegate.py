import sys

from twisted.internet import task, endpoints, defer, protocol
from twisted.python import log
from twisted.words.protocols import irc

from libs.util import EventEmitter
from nipplegate.ascii import AsciiHandler
from nipplegate.notes import NotesHandler
from nipplegate.quotes import QuoteCommands
from nipplegate.warframe import WarframeHandler


class NipplegateBot(irc.IRCClient, EventEmitter):
    nickname = "nipplegate"

    def __init__(self):
        super(NipplegateBot, self).__init__()
        self.deferred = defer.Deferred()
        self.registered_handlers = {
            'quote': QuoteCommands(self),
            'note': NotesHandler(self),
            'ascii': AsciiHandler(self),
            'wf': WarframeHandler(self)
        }

    def on_command(self, command, handler_func):
        self.registered_handlers[command] = handler_func

    def signedOn(self):
        self.join(self.factory.channel)

    def joined(self, channel):
        self.emit('join', channel)

    def privmsg(self, user, channel, message):
        nick, _, host = user.partition('!')
        message = message.strip()

        if not message.startswith('.'):
            self.emit('msg', message, nick)
            return

        command, sep, rest = message.lstrip('.').partition(' ')

        if command not in self.registered_handlers:
            return

        d = defer.maybeDeferred(self.delegate_message, command, rest, nick)

        d.addErrback(self._show_error)

        if channel == self.nickname:
            d.addCallback(self.send_message, nick)
        else:
            d.addCallback(self.send_message, channel)

    def send_message(self, msg, target):
        if isinstance(msg, list):
            for line in msg:
                self.msg(target, line.encode('ascii', 'ignore'))
        else:
            self.msg(target, msg.encode('ascii', 'ignore'))

    def _show_error(self, failure):
        return failure.getErrorMessage()

    def connectionLost(self, reason):
        self.deferred.errback(reason)

    def delegate_message(self, command, arguments, user):
        return self.registered_handlers[command].handle(arguments, user)


class NipplegateFactory(protocol.ReconnectingClientFactory):
    protocol = NipplegateBot
    channel = '#tetn leet'


def main(reactor, description):
    endpoint = endpoints.clientFromString(reactor, description)
    factory = NipplegateFactory()
    d = endpoint.connect(factory)
    d.addCallback(lambda protocol: protocol.deferred)

    return d


if __name__ == '__main__':
    log.startLogging(sys.stderr)
    task.react(main, ['tcp:irc.quakenet.org:6667'])
