import re
from collections import defaultdict

deleted_characters = ''.join(c for c in map(chr,range(256)) if not c.isalnum())


class EventEmitter(object):
    def __init__(self):
        self._events = defaultdict(list)

    def on(self, events, callback):
        if isinstance(events, list):
            for ev in events:
                self._events[ev].append(callback)
        else:
            self._events[events].append(callback)

    def emit(self, event, *args):
        for cb in self._events[event]:
            cb(*args)


def normalize_username(user_name):
    cleaned_user_name = user_name.translate(None, deleted_characters)
    cleaned_user_name = cleaned_user_name.lower()

    return cleaned_user_name


class CommandHandler(object):
    def __init__(self):
        self.commands = {
            r"^help$": self.help
        }

    def handle(self, arguments, user):
        for regex in self.commands.keys():
            if re.match(regex, arguments):
                rest = arguments.split(' ', 1)[-1]
                return self.commands[regex](rest, user)
        return "not something I recognize."

    def help(self, arguments, user):
        return ', '.join(map(self.clean_regex_key, self.commands.keys()))

    @staticmethod
    def clean_regex_key(key):
        return key.replace('^', '').replace('$', '') \
            .replace('[0-9]+-[0-9]+', '<id>-<id>') \
            .replace('[0-9]+', '<id>') \
            .replace('.*', ' <quote>') \
            .replace('[a-zA-Z0-9\s]+', '<text,numbers or spaces>')
